# The Maze App

# Introduction

Este proyecto está generado a través de '**yo angular generator**'(https://github.com/yeoman/generator-angular) version 0.11.1.


##Setup Project

En primer lugar, es necesario instalarse los paquetes bower y npm. A través de '**bower install**' y '**npm install**'.

## Build & development

Ejecutar '**grunt serve**' para lanzar la aplicación.


# About the development

## Architecture

Relativo a la arquitectura que se ha seguido, en la carpeta 'app' se pueden observar la estructura que sigue este proyecto. Destacan:

### Services
Continene todas las API's que se comuniquen con servicios externos.
En concreto, 'externalServiceApi' se encarga de llamadas puras a la Api, mientras que 'mazeApi' y 'userApi' se encargan de la comunicación entre la lógica de negocio de la aplicación y la 'externalServiceApi'.

De esta manera lo que se consigue es que cualquier cambio futuro que se pueda ocurrir en la API externa, afecte lo menos posible a la aplicación.

### Modules
Cada uno de los módulos es una ruta a la que se puede navegar (definida en el 'app.js'). Están compuestos por:

* **controller.js**: Se encarga de toda la lógica de negocio que se va a manejar en una ruta concreto.
* **template.html**: Template que se renderiza tras navegar a su ruta.
* **style.css**: Hoja de estilo específica de este módulo que es cargada desde el main.css.

### Styles
Contiene hojas de estilos genéricas que afectan a todo el proyecto.
Desde aquí se importan las hojas de estilo de los módulos que se deseen.

## Solve Maze

Para resolver el problema del laberinto, se ha seguido el planteamiento que se comentaba en el enunciado del mismo. Para ello, se han tenido en cuenta principalmente dos datos. Primero, conocer cuál es la dirección actual en la que nos estamos moviendo, el segundo, haber decidido si seguimos la pared la derecha o la de la izquierda.

Estos dos datos nos permiten saber cual es el orden de comprobaciones para elegir el siguiente movimiento que hay realizar.

Por ejemplo, con los siguientes datos

* dirección 'SOUTH'
* seguimos las paredes de la derecha

El orden de comprobaciones es:

* Is WEST disblocked?
* Is SOUTH disblocked?
* Is EAST disblocked?
* Is NORTH disblocked?

En cuanto se encuentra una de las direcciones libres, se debe mover a ella y actualizar la dirección en la que nos movemos. Es decir, si en el ejemplo, 
WEST y SOUTH están blocked, pero EAST está disblocked. Viajamos a EAST:

* dirección 'EAST'
* orden de prioridades: ['SOUTH', 'EAST', 'NORTH', 'WEST']


## CHALLENGES OBTAINED

Desarrollar mi primera aplicación web con cierta complejidad en AngularJS. Las sensaciones han sido inmejorables, ya que mi experiencia anterior había sido la de simples tutoriales, pero gracias a toda la comunidad de desarrolladores que existe, la gran potencia que posee AngularJS, junto a la experiencia previa en otros FrameworksJS (como Backbonejs + Marionettejs) han hecho que disfrute enormemente realizando este pequeño proyecto.