'use strict';

/**
 * @ngdoc function
 * @name theMazeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the theMazeApp
 */
angular.module('theMazeApp')
  .controller('MazeCtrl', function ($scope, mazeApi, userApi, $localStorage, $q) {
    

  	$scope.userData = $localStorage.userData;
  	$scope.playerSessionId = $localStorage.playerSessionId;
  	$scope.direcctionOrder = [mazeApi.WEST,  mazeApi.SOUTH, mazeApi.EAST, mazeApi.NORTH];
  	$scope.currentDirection = mazeApi.SOUTH;
  	$scope.solutionMessages = [];
  	$scope.cellVisitedArray = [];


  	var infoPlayer = {
		'playerSessionId': $scope.playerSessionId,
		'guid': $scope.userData.guid
	};

	var numberOfCells;

  	mazeApi.getMazeId($scope.userData.mazeId)
		.success(function(maze) {
	            console.log('data maze', maze);
	            $scope.map = maze;
				numberOfCells = maze.Nodes[maze.Nodes.length-1].CoordX + 1;
	            $scope.withCell = (numberOfCells * 25) + 'px';
	    })
	    .error(function(error) {
	        console.log('Error:' + error);
		}
	);

	$scope.executeAlgorithm = function() {
		_getPlayerStatus(infoPlayer).then(function(statusPlayer){
			_solveMaze(infoPlayer, statusPlayer);
	    });
	};

	function _solveMaze(infoPlayer, statusPlayer){
		console.log('statusPlayer', statusPlayer);
		var finished = statusPlayer.Finished;
			
		_nextMovement(infoPlayer, statusPlayer, 0).then(function(statusPlayer){
				if(!statusPlayer.Finished){
					_solveMaze(infoPlayer, statusPlayer);
				}else{
					console.log('Congratulation! You solve this Maze!');
					var timeStamp = new Date();
					$scope.solutionMessages.push({id: timeStamp, msg:'Congratulation! You solve this Maze!'});
					_drawSolution(infoPlayer.playerSessionId);
					
				}
			},function (error){
				console.log('error: ', error.Message);
				var error404 = 'The direction choosen is blocked¡¡¡¡¡';
				
				if(error.Message === error404){
					_solveMaze(infoPlayer, statusPlayer);
				}
			}		
		);
	};

	function _drawSolution(playerSessionId){
		_getPlayerSession(playerSessionId).then(function(playerSession){
			console.log('playerSession: ', playerSession.PositionHistory);

			playerSession.PositionHistory.forEach(function(position) {
  				var visitedId = (numberOfCells * position.CurrentY) + (position.CurrentX + 1)  ;
  				$scope.cellVisitedArray.push(visitedId);
			});

			console.log('cellVisitedArray: ', $scope.cellVisitedArray);
			console.log('solutionMessages: ', $scope.solutionMessages);
	    });
	};
	
	function _nextMovement(infoPlayer, statusPlayer, index){

		if(index < $scope.direcctionOrder.length){
		
			if(_checkDirection(statusPlayer, $scope.direcctionOrder[index])){				
				return _nextMovement(infoPlayer, statusPlayer, ++index);
			}else{	
				return _updateStatus(infoPlayer, statusPlayer, $scope.direcctionOrder[index]);
			}
		}
	}

	function _updateStatus(infoPlayer, statusPlayer, direction){
		switch(direction){
			case mazeApi.NORTH:
				$scope.direcctionOrder = [mazeApi.EAST, mazeApi.NORTH, mazeApi.WEST,  mazeApi.SOUTH];
				$scope.currentDirection = mazeApi.NORTH;
  				return _moveToNorth(infoPlayer, statusPlayer)

			case mazeApi.SOUTH:
				$scope.direcctionOrder = [mazeApi.WEST,  mazeApi.SOUTH, mazeApi.EAST, mazeApi.NORTH];
				$scope.currentDirection = mazeApi.SOUTH;
  				return _moveToSouth(infoPlayer, statusPlayer)

			case mazeApi.EAST:
				$scope.direcctionOrder = [mazeApi.SOUTH, mazeApi.EAST, mazeApi.NORTH, mazeApi.WEST];
				$scope.currentDirection = mazeApi.EAST;
  				return _moveToEast(infoPlayer, statusPlayer)

			case mazeApi.WEST:
				$scope.direcctionOrder = [mazeApi.NORTH, mazeApi.WEST,  mazeApi.SOUTH, mazeApi.EAST];
				$scope.currentDirection = mazeApi.WEST;
  				return _moveToWest(infoPlayer, statusPlayer)
		}
	}

	function _checkDirection(statusPlayer, direction){
		switch(direction){
			case mazeApi.NORTH:
				return statusPlayer.NorthBlocked;

			case mazeApi.SOUTH:
				return statusPlayer.SouthBlocked;

			case mazeApi.EAST:
				return statusPlayer.EastBlocked;

			case mazeApi.WEST:
				return statusPlayer.WestBlocked;
		}
	};

	//Get Current Status of player
	function _getPlayerStatus(infoPlayer){
		var deferred = $q.defer();

		userApi.getPlayerStatus(infoPlayer)
			.success(function(status){
				console.log('Current Status: ', status);
				 deferred.resolve(status); 
			})
  			.error(function(error){
  				console.log('Error:' + error);
  				 deferred.reject(error);
  			}
  		);
  		return deferred.promise;	
	};

	//Get Current Session of player
	function _getPlayerSession(playerSessionId){
		var deferred = $q.defer();

		userApi.getPlayerSession(playerSessionId)
			.success(function(status){
				console.log('Player Session: ', status);
				 deferred.resolve(status); 
			})
  			.error(function(error){
  				console.log('Error:' + error);
  				 deferred.reject(error);
  			}
  		);
  		return deferred.promise;	
	};

	//Movements in Maze
	function _moveToNorth(infoPlayer){
		var deferred = $q.defer();

		mazeApi.moveToNorth(infoPlayer)
			.success(function(status){
				console.log('Move to North: ', status);
				var timeStamp = new Date();
				$scope.solutionMessages.push({id: timeStamp, msg:'Move to North'});
				 deferred.resolve(status); 
			})
  			.error(function(error){
  				console.log('Error:' + error);
  				 deferred.reject(error);
  			}
  		);
  		return deferred.promise;	
	};

	function _moveToSouth(infoPlayer){
		var deferred = $q.defer();

		mazeApi.moveToSouth(infoPlayer)
			.success(function(status){
				console.log('Move to South: ', status);
				var timeStamp = new Date();
				$scope.solutionMessages.push({id: timeStamp , msg:'Move to South'});
				deferred.resolve(status); 
			})
  			.error(function(error){
  				console.log('Error:' + error);
  				 deferred.reject(error);
  			}
  		);
  		return deferred.promise;	
	};

	function _moveToWest(infoPlayer){
		var deferred = $q.defer();

		mazeApi.moveToWest(infoPlayer)
			.success(function(status){
				console.log('Move to West: ', status);
				var timeStamp = new Date();
				$scope.solutionMessages.push({id: timeStamp, msg:'Move to West'});
				deferred.resolve(status); 
			})
  			.error(function(error){
  				console.log('Error:' + error);
  				 deferred.reject(error);
  			}
  		);
  		return deferred.promise;	
	};

	function _moveToEast(infoPlayer){
		var deferred = $q.defer();

		mazeApi.moveToEast(infoPlayer)
			.success(function(status){
				console.log('Move to East: ', status);
				var timeStamp = new Date();
				$scope.solutionMessages.push({id: timeStamp, msg:'Move to East'});
				deferred.resolve(status); 
			})
  			.error(function(error){
  				console.log('Error:' + error);
  				 deferred.reject(error);
  			}
  		);
  		return deferred.promise;	
	};
  });