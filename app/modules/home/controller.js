'use strict';

/**
 * @ngdoc function
 * @name theMazeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the theMazeApp
 */
angular.module('theMazeApp')
  .controller('HomeCtrl', function ($scope, mazeApi, userApi, $localStorage, $location) {

  	$scope.user = {
        nick: '',
        mazeId: '',
        guid: ''
    };

    mazeApi.getMazeList()
		.success(function(mazeList) {
	            $scope.mazeList = mazeList;
	            $scope.user.mazeId = mazeList[0];
	        })
	    .error(function(error) {
	        console.log('Error:' + error);
	    });

	$scope.login = function() {

		/*var userData = {
  				"nick": 'CheckApi',
  				"mazeId": $scope.user.mazeId.Id,
  				"guid": 12345
		};
		$localStorage.userData = userData;
		$localStorage.playerSessionId = 108;
		$location.path('/maze');

*/
		var userData = {
  				"nick": $scope.user.nick,
  				"mazeId": $scope.user.mazeId.Id,
  				"guid": $scope.user.guid
		};

		userApi.login(userData)
			.success(function(playerSessionId) {
		            console.log('sessionId: ', playerSessionId);
		            $localStorage.userData = userData;
		            $localStorage.playerSessionId = playerSessionId;
		            $location.path('/maze');
		        })
		    .error(function(error) {
		        console.log('Error:' + error);
	    });
    };

  });
