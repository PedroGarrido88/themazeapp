'use strict';

angular.module('theMazeApp')
    .service('userApi', function($http, externalServiceApi) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var exports = {};
            
        exports.login = function(userData) {
            return externalServiceApi.postPlayerSession(userData);
        };

        exports.getPlayerStatus = function(userData) {
            return externalServiceApi.getPlayerStatus(userData);
        };

        exports.getPlayerSession = function(id) {
            return externalServiceApi.getPlayerSession(id);
        };

        return exports;
    });