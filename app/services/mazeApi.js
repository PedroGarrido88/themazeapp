'use strict';

angular.module('theMazeApp')
    .service('mazeApi', function($http, externalServiceApi) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var exports = {};
            exports.NORTH = 0,
            exports.SOUTH = 1,
            exports.WEST = 2,
            exports.EAST = 3;

        //GET MAZE
        exports.getMazeList = function() {
            return externalServiceApi.getMazeList();
        };

        exports.getMazeId = function(id) {
            return externalServiceApi.getMazeId(id);
        };

        //MOVEMENTS
        exports.moveToNorth = function(userData) {
            var data = _parseDataMovement(userData, this.NORTH);
            return externalServiceApi.postPlayerMovement(data);
        };

        exports.moveToSouth = function(userData) {
            var data = _parseDataMovement(userData, this.SOUTH);
            return externalServiceApi.postPlayerMovement(data);
        };

        exports.moveToWest = function(userData) {
            var data = _parseDataMovement(userData, this.WEST);
            return externalServiceApi.postPlayerMovement(data);
        };

        exports.moveToEast = function(userData) {
            var data = _parseDataMovement(userData, this.EAST);
            return externalServiceApi.postPlayerMovement(data);
        };

        function _parseDataMovement(userData, direction){
            return  {
                "PlayerSessionId": userData.playerSessionId,
                "Guid": userData.guid,
                "Direction": direction
            };
        }
        
        return exports;
    });