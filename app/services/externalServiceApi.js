'use strict';

angular.module('theMazeApp')
    .service('externalServiceApi', function($http) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var exports = {};
            

        var endPoint = 'http://178.33.162.233:28081/api/',
            mazeDefinition = 'MazeDefinition/',
            playerSession = 'PlayerSession/',
            playerMovement = 'PlayerMovement';

        //REQUEST TO MAZEDEFINITION
        exports.getMazeList = function() {
            return $http({
                method: 'GET',
                url: endPoint + mazeDefinition
            });
        };

        exports.getMazeId = function(id) {
            return $http({
                method: 'GET',
                url: endPoint + mazeDefinition + id
            });
        };        

        //REQUEST TO PLAYERSESSION
        exports.getPlayerSession = function(id) {
            return $http({
                method: 'GET',
                url: endPoint + playerSession + id
            });
        };
        
        exports.postPlayerSession = function(data) {
            return $http({
                method: 'POST',
                url: endPoint + playerSession,
                data : data
            });
        };

        //REQUEST TO PLAYERMOVEMENT
        exports.getPlayerStatus = function(userData) {
            return $http({
                method: 'GET',
                url: endPoint + playerMovement,
                params: {
                    playerSessionId: userData.playerSessionId,
                    guid: userData.guid
                }
            });
        };
        
        exports.postPlayerMovement = function(data) {
            return $http({
                method: 'POST',
                url: endPoint + playerMovement,
                data : data
            });
        };

        return exports;
    });