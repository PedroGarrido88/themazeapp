'use strict';

/**
 * @ngdoc overview
 * @name theMazeApp
 * @description
 * # The Maze App
 *
 * Main module of the application.
 */
angular
  .module('theMazeApp', [
    'ngRoute',
    'ngStorage'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'modules/home/template.html',
        controller: 'HomeCtrl'
      })
      .when('/maze', {
        templateUrl: 'modules/maze/template.html',
        controller: 'MazeCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
